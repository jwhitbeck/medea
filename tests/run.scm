(use test medea)

(define-syntax test-read
  (syntax-rules ()
    ((_ result json)
     (test-read json result json))
    ((_ description result json)
     (test description result (read-json json)))))

(define-syntax test-write
  (syntax-rules ()
    ((_ result doc)
     (test-write result result doc))
    ((_ description result doc)
     (test description result (with-output-to-string (lambda () (write-json doc)))))))

(load-relative "reading")
(load-relative "writing")
(load-relative "parsers-unparsers")

(test-exit)